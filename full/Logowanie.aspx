﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Logowanie.aspx.cs" Inherits="Logowanie" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <span id="span">
                <asp:Label ID="Label1" runat="server" Text="Numer NIU"></asp:Label>
            </span>
            <asp:TextBox ID="TextBox1" runat="server" Text="" CssClass="text1"></asp:TextBox>
        </div>
        <div>
            <span id="span">
                <asp:Label ID="Label2" runat="server" Text="Hasło "></asp:Label>
            </span>
            <asp:TextBox ID="haslo" runat="server" Text="" type="password" CssClass="text2"></asp:TextBox>

        </div>
        <div>
            <asp:Button ID="Zaloguj" runat="server" Text="Zaloguj" OnClick="Button1_Click" />
        </div>
    <style>
        .text1 {
            margin-left: 55px;
        }
        .text2 {
            margin-left: 95px;
        }
        #span{
            margin-left: 10px;
        }
    </style>
        <asp:Label ID="Label3" runat="server" Font-Size="Small" Text="Nie udało ci się zalogować, spróbuj jeszcze raz." Visible="false"></asp:Label>
    </form>
    
</body>
</html>
