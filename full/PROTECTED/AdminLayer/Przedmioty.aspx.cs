﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PROTECTED_AdminLayer_Przedmioty : System.Web.UI.Page
{
    private static string path = HttpContext.Current.Server.MapPath("~/App_Data/DziekanatDB.mdf");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PolaczPrzedmiot();
        }
    }

    protected void PolaczPrzedmiot()
    {
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("select * from Przedmioty");
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        GridView_Przedmioty.DataSource = dt;
        GridView_Przedmioty.DataBind();
        GridView_Przedmioty.Visible = true;
        if (dt.Rows.Count != 0)
        {
            Label_brak_rekordu.Text = "";
        }
        else
        {
            Label_brak_rekordu.Text = "Brak rekordów";
        }
        connection.Close();
    }

    protected void SzukajPrzedmiot()
    {
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("select * from Przedmioty where nazwa like '%{0}%'",TextBox_Szukaj_Przedmiot.Text);
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        GridView_Przedmioty.DataSource = dt;
        GridView_Przedmioty.DataBind();
        GridView_Przedmioty.Visible = true;
        if (dt.Rows.Count == 0)
        {
            Label_brak_rekordu.Text = "Brak rekordu";
        }
        else
        {
            Label_brak_rekordu.Text = "";
        }
        connection.Close();
        
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView_Przedmioty.EditIndex = e.NewEditIndex;
        PolaczPrzedmiot();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView_Przedmioty.EditIndex = -1;
        PolaczPrzedmiot();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        String id = GridView_Przedmioty.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("delete from Przedmioty where Id='{0}'", id);
        command.CommandType = CommandType.Text;
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        PolaczPrzedmiot();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        String id = GridView_Przedmioty.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        for (int i = 0; i < GridView_Przedmioty.Columns.Count; i++)
        {
            DataControlFieldCell objCell = (DataControlFieldCell)GridView_Przedmioty.Rows[e.RowIndex].Cells[i];
            GridView_Przedmioty.Columns[i].ExtractValuesFromCell(e.NewValues, objCell, DataControlRowState.Edit, false);
        }
        bool blad=false;
        try
        {
            int wyk_l_godz = Convert.ToInt32(e.NewValues["wyk_l_godzin"]);
            int lab_l_godz = Convert.ToInt32(e.NewValues["lab_l_godzin"]);
        }
        catch(Exception ex)
        {
            blad = true;
        }
        if (e.NewValues["rodzaj"].ToString().Length == 0 || e.NewValues["nazwa"].ToString().Length == 0 || e.NewValues["wyk_l_godzin"].ToString().Length == 0 || e.NewValues["lab_l_godzin"].ToString().Length == 0 )
        {
            blad = true;
        }
        if (!blad)
        {
            command.CommandType = CommandType.Text;
            command.CommandText = String.Format("update Przedmioty set nazwa='{1}', wyk_l_godzin='{2}', lab_l_godzin='{3}', egzamin='{4}', rodzaj='{5}' where id='{0}'", id, e.NewValues["nazwa"], e.NewValues["wyk_l_godzin"], e.NewValues["lab_l_godzin"], e.NewValues["egzamin"], e.NewValues["rodzaj"]);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
            GridView_Przedmioty.EditIndex = -1;
            PolaczPrzedmiot(); 
            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + "Pomyślnie zaktualizowano" + "');", true);
        }
        else
        {

            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + "Wystąpił błąd" + "');", true);
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Przedmioty.PageIndex = e.NewPageIndex;
        PolaczPrzedmiot();
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Server.Transfer("DodajPrzedmiot.aspx");
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        SzukajPrzedmiot();
    }
}