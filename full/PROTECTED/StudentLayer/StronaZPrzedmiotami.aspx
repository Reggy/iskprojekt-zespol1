﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/PROTECTED/StudentLayer/MasterStudent.master" CodeFile="StronaZPrzedmiotami.aspx.cs" Inherits="PROTECTED_StudentLayer_StronaZPrzedmiotami" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div>
            <asp:GridView ID="GridView1" runat="server" Visible="False" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" EnableModelValidation="True">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:BoundField DataField="idprzedmiotu" HeaderText="Przedmiot" />
                    <asp:BoundField DataField="semestr" HeaderText="Ocena" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
        </div>
</asp:Content>
